//***********************************************************************************************************************************
#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <string>
//***********************************************************************************************************************************
#define STACKSIZE     4096  // ������ ����� ������
#define ArrayS_Size   30    // ����������� ��������
#define W_Width       140   // ������ ������ 
#define W_Heigth      60    // ������ ������
#define C_PosX        50    // ������� � ������ �����
#define C_PosY        30    // ������� � ������ ����� 
#define C_Rad         15    // ������ ����� �������� �����
#define TaxiPosCount  5     // ����������� �����
#define TaxiDelay     1500  // �������� �������� �� ���������
//***********************************************************************************************************************************
DWORD WINAPI Taxi();
DWORD WINAPI Passazhir(LPVOID lpParam);
CRITICAL_SECTION cs;
long Position;
int S;
int BS_Count[ArrayS_Size];
HANDLE  hout,                     // ����� �� ������� ������
EvStop[5],                // ������ ������� �������: ���������1-���������4
hTaxi,                    // ����� �����
T_Sem,                    // ����� �������� �����
hPassazhir[ArrayS_Size],  // ������ ������� ����������
hSemaphore[4];            // ������ ������� �������� ���������
						  //***********************************************************************************************************************************
void PrintMess(int x, int y, const char * Mess, int Param, int BackGround, int ForeGround)
{
	COORD cursor_pos;
	cursor_pos.X = x;
	cursor_pos.Y = y;
	EnterCriticalSection(&cs);        //������ ����������� ������(����� �� ����� ������������ ������ ���� ����� )
	SetConsoleCursorPosition(hout, cursor_pos);   // ������������� ������� �������  
	//textbackground(BackGround);                   // ������ ���� ����
	//textcolor(ForeGround);                        // ���� ����
	printf(Mess, Param);                         // ������� ��������� �� ����� �������
	LeaveCriticalSection(&cs);                  // ����� ����������� ������
}
//***********************************************************************************************************************************

//***********************************************************************************************************************************
void DrawStopBus(void)
{
	PrintMess(C_PosX - C_Rad + 5, C_PosY, "BS1", NULL, FOREGROUND_RED, 3);
	PrintMess(C_PosX, C_PosY - C_Rad - 2, "BS2", NULL, FOREGROUND_RED, 3);
	PrintMess((C_PosX + C_Rad - 5)*1.4, C_PosY, "BS3", NULL, FOREGROUND_RED, 3);
	PrintMess(C_PosX, C_PosY + C_Rad + 1, "BS4", NULL, FOREGROUND_RED, 3);
}
//***********************************************************************************************************************************
void Init(void)
{
	SMALL_RECT ConsoleWindow;
	COORD coord;
	CONSOLE_CURSOR_INFO cci;
	InitializeCriticalSection(&cs);
	int randomize();                                        //������������ �������� 
	hout = GetStdHandle(STD_OUTPUT_HANDLE);              // ����� �� ���� �������
	ConsoleWindow.Left = 0;
	ConsoleWindow.Top = 0;
	ConsoleWindow.Right = W_Width;
	ConsoleWindow.Bottom = W_Heigth;
	SetConsoleWindowInfo(hout, TRUE, &ConsoleWindow);    //������ ���� ������ ������� 
	coord.X = W_Width;
	coord.Y = W_Heigth;
	SetConsoleScreenBufferSize(hout, coord);             // ������ ������ ������ �������
	cci.dwSize = 99;
	cci.bVisible = FALSE;
	SetConsoleCursorInfo(hout, &cci);                     // ������� �������� ������
	do {                                                 // ��������� ���� ���� �� ����� ������� ���������� ��������

		system("cls");                                     //  ������� ������ �������  
		PrintMess(5, 1, "������� ����� ���������� ���������� ������ 20)# ", NULL, 0, 2);
		scanf("%d", &S);
	} while (S<1 || S>20);
	PrintMess(25, W_Heigth - 2, "��� ���������� ��������� ������� ����� �������", NULL, 0, 12);
	//****** ������������ ���������*********************************************************************************************
	DrawStopBus();
}
//***********************************************************************************************************************************
int main()
{
	int i;
	DWORD dwThreadID;
	Init();                                                     // �������������
	for (i = 1; i <= 4; i++)
		BS_Count[i] = 0;
	for (i = 1; i <= 4; i++)
	{
		EvStop[i] = CreateEvent(NULL, TRUE, FALSE, NULL); // ������� ������� ��� ������������ ��������������� �����
		hSemaphore[i] = CreateSemaphore(NULL, 1, S*S, NULL);      //������� ��������-�������� ��� ���������          
	}
	// ������� ����� ���������� �� �������� � ������ �����
	hTaxi = CreateThread(NULL, STACKSIZE, (unsigned long(__stdcall *)(void *))Taxi, NULL, NULL, &dwThreadID);
	for (i = 1; i <= S; i++)
	{
		// ������� ������ ����������
		hPassazhir[i] = CreateThread(NULL, STACKSIZE, Passazhir, (void*)i, NULL, &dwThreadID);
		BS_Count[i] = -1;
		Sleep(2 * (rand() % 6 + 4));                                    // ���������� ������� ����� ��������� �����
	}
	getch();                                                    // ���������� �������� ��������� ����� �������
	system("cls");                                              // ������� ������ �������
	for (i = 1; i <= 4; i++)
	{
		CloseHandle(EvStop[i]);
		CloseHandle(hSemaphore[i]);
		CloseHandle(T_Sem);
	}
	return 0;
}
//***********************************************************************************************************************************
DWORD WINAPI Taxi()
{
	char Str[300], B_Str1[300], B_Str2[300], B_Str3[300], B_Str4[300];
	int i, k;
	float angle = -180;                                   //  ������ ��������� ��������� �����
	T_Sem = CreateSemaphore(NULL, TaxiPosCount, S, NULL); // �������������� ������� ��� �������� ���������� ���������� � �����
	InterlockedExchange(&Position, 4);                      // �������������� ��������� ������� �� 4 
															//*****������ �������������� ��������� �����***************************************************************************
	PrintMess(C_Rad * cos(angle * 3.14 / 180) * 1.5 + C_PosX, C_Rad * sin(angle * 3.14 / 180) * 1.0 + C_PosY, "Taxi", NULL, 0, 7);
	//****** ����������� ���� �������� �����*********************************************************************************
	while (TRUE)
	{
		if (!ResetEvent(EvStop[Position]))               // ������������� ���� ���������
			
		InterlockedExchangeAdd(&Position, 1);           // ����������� �������� Position++
		InterlockedCompareExchange(&Position, 1, 5);    // �osition=1, ���� ������ 4
		if (!SetEvent(EvStop[Position]))                 //������������� ������� �� �������� �� N-�� ���������
			
		Sleep(TaxiDelay);                               // �������� �������� ����� �� ���������
		angle += 1;                                     // ��������� ����
														//****** ��������� ������ ���������� � ����� *********************************************************************
		memset(Str, 0, sizeof(Str));
		for (k = 1; k <= S; k++)
			if (BS_Count[k] == 777)
			{
				char buffer[20] = "";
				itoa(k, buffer, 10);
				strcat(Str, strcat(buffer, " "));
			}
		//****** ��������� ������ ���������� �� ��������� *******************************************************************
		memset(B_Str1, 0, sizeof(B_Str1));
		memset(B_Str2, 0, sizeof(B_Str2));
		memset(B_Str3, 0, sizeof(B_Str3));
		memset(B_Str4, 0, sizeof(B_Str4));
		for (i = 1; i <= S; i++)
		{
			char buffer[20] = "";
			itoa(i, buffer, 10);
			switch (BS_Count[i])
			{
			case 1: strcat(B_Str1, strcat(buffer, " ")); break;
			case 2: strcat(B_Str2, strcat(buffer, " ")); break;
			case 3: strcat(B_Str3, strcat(buffer, " ")); break;
			case 4: strcat(B_Str4, strcat(buffer, " ")); break;
			}
		}
		PrintMess(C_PosX - C_Rad + 5, C_PosY + 1, B_Str1, NULL, 0, 7);
		PrintMess(C_PosX, C_PosY - C_Rad - 1, B_Str2, NULL, 0, 7);
		PrintMess((C_PosX + C_Rad - 5)*1.4, C_PosY + 1, B_Str3, NULL, 0, 7);
		PrintMess(C_PosX, C_PosY + C_Rad + 2, B_Str4, NULL, 0, 7);
		//******���� ���� ������ 90, �.�. 90, 180, 270, 360, 450 � �.�. ***********************************************************
		while ((int)angle % 90 != 0)
		{
			//**********************************************�������� ������ ��������� �����****************************************
			PrintMess(C_Rad * cos((angle - 1) * 3.14f / 180) * 1.5 + C_PosX, C_Rad * sin((angle - 1) * 3.14f / 180) * 1.0 + C_PosY, "    ", NULL, 0, 7);
			PrintMess(C_Rad * cos((angle - 1) * 3.14f / 180) * 1.5 + C_PosX, C_Rad * sin((angle - 1) * 3.14f / 180) * 1.0 + C_PosY + 1, "            ", NULL, 0, 7);
			angle += 1;                                   // ��������� ���� 
														  //********************************************** ������ ����� ��������� �����*******************************************
			PrintMess(C_Rad * cos(angle * 3.14f / 180) * 1.5 + C_PosX, C_Rad * sin(angle * 3.14f / 180) * 1.0 + C_PosY, "Taxi", NULL, 0, 7);
			PrintMess(C_Rad * cos(angle * 3.14f / 180) * 1.5 + C_PosX, C_Rad * sin(angle * 3.14f / 180) * 1.0 + C_PosY + 1, Str, 0, 2, 7);
			Sleep(10);                                    // �������� ��������
		}
		DrawStopBus();
	}
	return 0;
}
//***********************************************************************************************************************************
DWORD WINAPI Passazhir(LPVOID lpParam)
{
	HANDLE hEvents[1];
	long lplPreviousCount = 0;
	int PassPosition, EndPosition, WasteTime,
		ID_Thread = (int)lpParam,
		Color = BACKGROUND_GREEN + rand() % 1000;
	while (TRUE)
	{
		WasteTime = rand() % 10000; // ����� �������� ����� ��������� ����������
		PrintMess(90, 1 + ID_Thread, "������ %d ������                       ", WasteTime / 1000, 0, FOREGROUND_RED);
		Sleep(WasteTime);
		//*** ������ ��������� ������� ������� � �������� ��������� *************
		PassPosition = 1 + rand() % 4;
		while (Position == PassPosition)
			PassPosition = 1 + rand() % 4;
		EndPosition = (PassPosition + rand() % 3) % 4 + 1;

		BS_Count[ID_Thread] = PassPosition; // ������� ��� �������� ����� � � �������
											//***********************************************************************
		PrintMess(90, 1 + ID_Thread, "����� � ������� �� %d-�� ���������                  ", PassPosition, 0, 7);
		WaitForSingleObject(hSemaphore[PassPosition], INFINITE);  // ������ ������� �� ���������

		PrintMess(90, 1 + ID_Thread, "������ � ������� �� ��������� %d. ���� �����        ", PassPosition, 0, 7);
		//***** ������� ����� �� ��������� PassPosition � ������� � ��� ���������� �����
		hEvents[0] = EvStop[PassPosition];
		hEvents[1] = T_Sem;
		WaitForMultipleObjects(2, hEvents, TRUE, INFINITE);

		BS_Count[ID_Thread] = 777;  // ������� ���� ��� �������� ���

									// ��� ���� � ����� ����������� ����� �� ��������� ��� ���������� ��������� 
		ReleaseSemaphore(hSemaphore[PassPosition], 1, NULL);

		PrintMess(90, 1 + ID_Thread, "����. ����                                     ", EndPosition, 0, 8);
		// ���� �� �������� ���������
		WaitForSingleObject(EvStop[EndPosition], INFINITE);

		BS_Count[ID_Thread] = -1; // ������� ��� �������� ��� �� ������

		ReleaseSemaphore(T_Sem, 1, NULL);
		PrintMess(90, 1 + ID_Thread, "�������. �������                                     ", NULL, 0, 1);
		Sleep(1000);
	}
	return 0;
}
//****************************************************************
